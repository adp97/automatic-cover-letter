""" This script is intended for autogenerate cover letters given a custom template """ 
__author__="Angel del Pino Jimenez"

import os
import string

# Your name and surname (EDIT 0)
name = 'Angel'
first_surname = 'delPino'
second_surname = 'Jimenez'

full_name = first_surname + '_' + second_surname + '_' + name

# Cover letter template .tex
print('Cover letter template: ')
file_name = input()

try: # .tex extension checker
    file_name.index('.')
    if file_name.endswith('.tex'):
        file_name = file_name[:file_name.find('.')]
    else:
        raise Exception("Just .tex files are supported")
except ValueError:
    pass

ext='.tex'

if os.path.exists(file_name + ext): # .tex file finder
    pass
else:
    raise Exception(".tex file cannot be found, choose a new one")

# Input variables for changing the specific position (EDIT 1)
print('Company name: ')
COMPANY_NAME = input()
print('Company address: ')
COMPANY_ADDRESS = input()
print('Company position: ')
COMPANY_POSITION = input()

# New .tex file
file_name_new = file_name + '_' + COMPANY_NAME + ext
file_name_new = file_name_new.replace(' ','_') # Delete whitespaces in company name
open(file_name_new,'w+')

# Open .tex file
file_name = file_name + ext
with open(file_name, 'r') as file :
  filedata = file.read()

# Find variables (EDIT 2)
comp_name = "COMPANY_NAME_TEXT"
comp_address = "COMPANY_ADDRESS_TEXT"
comp_position = "COMPANY_POSITION_TEXT"

# New text to replace (EDIT 3)
comp_name_new =  COMPANY_NAME
comp_address_new = COMPANY_ADDRESS
comp_position_new = COMPANY_POSITION

filedata = filedata.replace(comp_name,comp_name_new)
filedata = filedata.replace(comp_address,comp_address_new)
filedata = filedata.replace(comp_position,comp_position_new)

# Write on .tex file
with open(file_name_new, 'w') as file:
  file.write(filedata)

# PDF creation calling pdflatex from MiKTeX
pdf =  'pdflatex' + ' ' + file_name_new
os.system(pdf)

# Delete previous .pdf file of the same company
try: 
    open('Cover Letters/' + full_name + '_' +  file_name_new[:file_name_new.find('.')] + '.pdf')
    os.remove('Cover Letters/' + full_name + '_' +  file_name_new[:file_name_new.find('.')] + '.pdf')  
    print('Previous pdf deleted, creating new one')
except IOError:
    print('No previous pdf found, creating new one')

# Delete all files except .pdf and .tex
os.remove( file_name_new[:file_name_new.find('.')] + '.aux')
os.remove( file_name_new[:file_name_new.find('.')] + '.log')
os.remove( file_name_new[:file_name_new.find('.')] + '.out')
#os.remove( file_name_new[:file_name_new.find('.')] + '.tex') # Commented out, in case there are some other changes to be made

# Rename pdf with name and surname and move to folder
try:
    os.mkdir('Cover Letters')
except:
    pass

os.rename(r'' + file_name_new[:file_name_new.find('.')] +'.pdf' , 'Cover Letters/' + full_name + '_' +  file_name_new[:file_name_new.find('.')] + '.pdf')
