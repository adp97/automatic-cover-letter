<!-- ABOUT THE PROJECT -->
## About The Project

If you are tired of creating +30 cover letters to different companies or institutions where more than 70% are ignored, this project is for you. Create a template of your cover letter in LaTeX format and this script automatically replaces the name of the institution and the name of the position you are applying to. 

<div align="center">
    <img src="cover_sample.jpg" class="center" alt="alt text" width="556 height="229">
</div>


<!-- GETTING STARTED -->
### Prerequisites

* LaTeX compiler installation in your pc. I have been using for years [https://www.texstudio.org/](https://www.texstudio.org/), but any other compiler would be fine too. 

* Python installed. This script has been tested with Python 3.9

* Your cover letter template created in .tex format

### Installation
Very simple:
1. Clone the repo
   ```sh
   git clone https://gitlab.com/adp97/automatic-cover-letter
   ```
And that's all

<!-- USAGE EXAMPLES -->
## Usage
0. Prepare your .tex template with your custom variables (further details on complete custom template in the next section)

1. Change full_name parameter by opening code.py using any text editor (EDIT 0 localizer)
    ```py
    # Your name and surname (EDIT 0)
    name = 'Angel'
    first_surname = 'delPino'
    second_surname = 'Jimenez'
    ```

2. Open cmd terminal in current repo directory

3. Call main code python script
   ```py
   python code.py
   ```

4. Use prompt input messages to fullfill the variable names
    ``` 
    >> Cover letter template:
    cover_letter

    >> Company name:
    Fake Company

    >> Company address:
    Invented City, \\ Invented Town, 19 30 \\ PC 1919

    >> Company position:
    Invented position
    ```

5. Check Cover Letters directory to find your recently created pdf

<!-- CUSTOM COVER LETTER -->
## Create yout own custom Cover Letter
1. In code.py, declare variables you will later fill with the desired company information (EDIT 1 localizer)
    ```py
    # Input variables for changing the specific position (EDIT 1)
    print('Company name: ')
    COMPANY_NAME = input()
    print('Company address: ')
    COMPANY_ADDRESS = input()
    print('Company position: ')
    COMPANY_POSITION = input()
    ```

2. In code.py, declare new variables for looking for them in the .tex file. This are the ones you may use later in your .tex file (EDIT 2 localizer)
    ```py
    # Find variables (EDIT 2)
    comp_name = "COMPANY_NAME_TEXT"
    comp_address = "COMPANY_ADDRESS_TEXT"
    comp_position = "COMPANY_POSITION_TEXT"
    ```

3. In .tex, use previous variables to be replaced later. You can place them wherever. 
    ```
    \recipient{COMPANY_NAME_TEXT}{COMPANY_ADDRESS_TEXT}
    \date{\today} 
    \opening{Dear Hiring Committee,}
    \closing{I appreciate the given time to review my cover letter.} 
    \makelettertitle

    With this letter, I would like to express my interest in the internship opportunity of the \textit{COMPANY_POSITION_TEXT} topic.
    ```

4. In code.py, specify the variables declared before to be replaced (EDIT 3 localizer)

    ```py 
    # New text to replace (EDIT 3)
    comp_name_new =  COMPANY_NAME
    comp_address_new = COMPANY_ADDRESS
    comp_position_new = COMPANY_POSITION

    filedata = filedata.replace(comp_name,comp_name_new)
    filedata = filedata.replace(comp_address,comp_address_new)
    filedata = filedata.replace(comp_position,comp_position_new)
    ```
You are done, go back to Usage section
<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See [LICENSE.md](LICENSE.md) for more information.


<!-- CONTACT -->
## Contact

Angel del Pino Jimenez - [@angeldpino](https://twitter.com/angeldpino) - angel.delpino97@gmail.com

Project Link: [https://gitlab.com/adp97/repo_name](https://gitlab.com/adp97/automatic-cover-letter)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* Thanks to my university colleges for giving me the idea 


---
Con ❤️ por [Angel del Pino](https://github.com/adp97) 😊
